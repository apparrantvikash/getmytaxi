@extends('master')
@section('content')
    <section class="page-title" style="background-image:url(images/background/3.jpg);">
        <div class="auto-container">
            <h1>ABOUT GET MY TAXI</h1>
            <div class="bread-crumb-outer">
                <ul class="bread-crumb clearfix">
                    <li><a href="{{route('airport')}}">Home</a></li>
                    <li class="active">About Get My Taxi</li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->
    
	<section class="experiance-section" style="padding: 50px 0px 0px;">
    	<div class="auto-container">
        	<div class="row clearfix">
            	<!--content column-->
            	<div class="content-column col-md-12 col-sm-12 col-xs-12">
                	<div class="inner-box">
                    	<h3 style="text-align:center;"><a href="#">WE PROMISE, YOU WILL HAVE THE BEST EXPERIENCE</a></h3>
                        <div class="text">
                        	<p style="text-align: justify;">Get My Taxi is a Bangalore based company working towards excellence in the Transportation, and believe in pursuing business through innovation and technology. Our team comes with several years of industry experience. We are a company that is dedicated to establish good business relationship with our clients giving them value for their money and reasons for them to rent and lease our cars over and over again. Established players in the travel industry offering specialized quality services to customers at affordable cost and personalized leasing options and competitive pricing for daily, weekly, monthly rental vehicles.</p>
                        </div>
						
						<h3 style="text-align:center;"><a href="#" style="text-transform: uppercase;
    color: #f2b341;">Vision And Mission</a></h3>
                        <div class="text">
                        	<p style="text-align: justify;">To be one of the pioneers in the ground transportation business through our quality service, wide range of networks. To provide the highest quality service to our customers by continuously incre - asing cost efficiency and maintaining delivery deadlines. To encourage our workforce to continuously strive for quality and excellence in everything we do. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endsection