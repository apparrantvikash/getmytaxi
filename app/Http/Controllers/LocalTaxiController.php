<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Localtaxi;
class LocalTaxiController extends Controller
{
    function index(){
    	return view('Local');
    }
    function localTaxi(Request $request){
    	$taxis = Localtaxi::orderBy('price' , 'ASC')->get();
    	return view('Local-Taxi' , ['taxis'=>$taxis , 'request'=>$request]);
    }
}
