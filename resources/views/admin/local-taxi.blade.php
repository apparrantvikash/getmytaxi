@extends('admin.layout.master')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Local Taxi</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a></li>
              <li class="breadcrumb-item active">Local Taxi</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">

            <div class="col-md-12">
                  <!-- @if(request()->page == 'add') @endif -->
              <div class="card">
                <div class="card-header">
                  <h3 class="card-title">Local Taxi</h3>

                  <div class="card-tools">
                    <a href="{{route('addlocaltaxi')}}" class="btn btn-primary"> <i class="fa fa-plus"></i> Add New</a>
                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0">
                @if(session()->has('message'))
                <div class="alert alert-success">
                {{ session()->get('message') }}
                </div>
                @endif 
                @if(session()->has('danger'))
                <div class="alert alert-danger">
                {{ session()->get('danger') }}
                </div>
                @endif 
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>S NO.</th>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Capacity</th>
                    <th>Price</th>
                    <th>Total Duration (Hours)</th>
                    <th>Total Distance (Kilometers) </th>
          <th>Extra (KM Price)</th>
          <th>Extra (hour Price)</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
@foreach($taxis as $key=>$taxi)
                  <tr>
                    <td>{{$key+1}}</td>
          <td><img src="{{asset('storage/'.$taxi->img)}}" width="100px"></td>
                    <td>{{$taxi->name}}</td>
                    <td>{{$taxi->capacity}}</td>
                    <td><i class="fa fa-rupee"></i> {{$taxi->price}}</td>
                    <td> {{$taxi->tdh}}  HR</td>
                    <td> {{$taxi->tdk}} KM</td>
                    <td><i class="fa fa-rupee"></i> {{$taxi->ekp}} </td>
                    <td><i class="fa fa-rupee"></i> {{$taxi->ehp}} </td>
                    <td>
          <a href="{{route('editlocaltaxi' , ['id'=>$taxi->id])}}" class="btn btn-info btn-block btn-sm">Edit</a>
          <a href="{{route('deletelocaltaxi' , ['id'=>$taxi->id])}}" class="btn btn-danger btn-block btn-sm" onclick="return confirm('Are you sure want to delete this?')">Delete</a>
          </td>
                  </tr> 
@endforeach
                  </tbody>
                </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @endsection