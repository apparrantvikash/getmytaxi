@extends('admin.layout.master')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Local Taxi</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a></li>
              <li class="breadcrumb-item active">Local Taxi</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-12 col-12">
      <div class="card card-warning">
              <div class="card-header">
                <h3 class="card-title">Add Local Taxi</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="{{route('storelocaltaxi')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
          <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
              <center>
                            <label>Image</label>
                            <img id="profileimage" src="{{asset('storage/gallery/imageplaceholder.png')}}" class="img-thumbnail img-responsive" style="width:320px; height:200px;"><br />
                            <div  style=" width:320px; padding-top: 10px; padding-bottom: 10px;">
                                <div class="fileUpload btn btn-info btn-block">
                                <span><strong>Browse &nbsp; </strong><i class="glyphicon glyphicon-folder-open"></i></span>
                                <input name="img" type="file" class="upload" onchange="readURL(this);" required />
                </div>
                <span>Image size should be (300*130) px</span>    
                            </div>
                 @if($errors->has('img'))
                    <div class="error text-danger">{{ $errors->first('img') }}</div>
                 @endif            
              </center>                                
            </div>
            <div class="col-md-4"></div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Taxi Name</label>
                <input type="text" class="form-control" name="name" placeholder="Taxi Name" value="{{old('name')}}" >
              </div>
              @if($errors->has('name'))
                    <div class="error text-danger">{{ $errors->first('name') }}</div>
              @endif  
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Taxi Capacity</label>
                <input type="text" class="form-control" name="capacity" placeholder="Taxi Capacity" value="{{old('capacity')}}" >
              </div>
              @if($errors->has('capacity'))
                    <div class="error text-danger">{{ $errors->first('capacity') }}</div>
              @endif 
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Price</label>
                <input type="number" class="form-control" name="price" placeholder="Price" value="{{old('price')}}" >
              </div>
              @if($errors->has('price'))
                    <div class="error text-danger">{{ $errors->first('price') }}</div>
              @endif 
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Total Duration( in hours)</label>
                <input type="number" class="form-control" name="tdh" placeholder="Total Duration( in hours)" value="{{old('tdh')}}" >
              </div>
              @if($errors->has('tdh'))
                    <div class="error text-danger">{{ $errors->first('tdh') }}</div>
              @endif 
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Total Distance( in kilometers)</label>
                <input type="number" class="form-control" name="tdk" placeholder="Total Distance( in kilometers)" value="{{old('tdk')}}" >
              </div>
              @if($errors->has('tdk'))
                    <div class="error text-danger">{{ $errors->first('tdk') }}</div>
              @endif 
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Extar Hour Price</label>
                <input type="number" class="form-control" name="ehp" placeholder="Extar Hour Price" value="{{old('ehp')}}" >
              </div>
              @if($errors->has('ehp'))
                    <div class="error text-danger">{{ $errors->first('ehp') }}</div>
              @endif 
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Extra Kilometr Price</label>
                <input type="number" class="form-control" name="ekp" placeholder="Extra Kilometr Price" value="{{old('ekp')}}" >
              </div>
              @if($errors->has('ekp'))
                    <div class="error text-danger">{{ $errors->first('ekp') }}</div>
              @endif 
            </div>
          </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                 <center> <button type="submit" name="add" class="btn btn-primary">Submit</button></center>
                </div>
              </form>
        

        
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->

      </div><!-- /.container-fluid -->
    </section>
  </div>
  <!-- /.content-wrapper -->
  @endsection