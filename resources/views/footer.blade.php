<section class="contact">
    <div class="auto-container">
		<center><!--<h1>Get My Taxi</h1>--> 
		<div class="row">
		<div class="col-md-4"></div>
		<div class="col-md-4"><img src="{{asset('images/logo.jpeg')}}" width="100%"></div>
		<div class="col-md-4"></div>
		</div>
		</center>
		<div class="eah">
			<div class="row">
			<div class="col-md-4">
				<div class="eah1">
					<span><i class="fa fa-check"></i></span>
					<span> 
					<h3> Verified Drivers</h3> 
					<!--<p>Reliable, Verified & Professional Drivers</p>-->
					</span>
				</div>
			</div>
			<div class="col-md-4">
				<div class="eah1">
					<span><i class="fa fa-calendar"></i></span>
					<span> 
					<h3>24/7 Uptime Customer Care</h3> 
					<!--<p>24x7 Dedicated Helpline</p>-->
					</span>
				</div>
			</div>
			<div class="col-md-4">
				<div class="eah1">
					<span><i class="fa fa-smile-o"></i></span>
					<span> 
					<h3>1000+ Customers</h3> 
					<!--<p>Served 10000+ Happy Customers</p>-->
					</span>
				</div>
			</div>
			</div>
		</div>
		<div class="">
			<div class="row">
				<div class="col-md-4">
					<div class="address">
						<p><i class="fa fa-location-arrow"></i> #170, 16th Cross Rd, 7th B Main RdJP Nagar 4t  PhaseBangalore, Karnataka 560078 </p>
						<p><i class="fa fa-phone"></i> <a href="tel:+918884488255 "> +918884488255 </a> / <a href="tel:+918884488355"> +918884488355 </a></p>
						<p><i class="fa fa-link"></i> <a href="http://getmytaxi.in/"> www.getmytaxi.in</a></p>
						<p><i class="fa fa-envelope"></i>  <a href="mailto:booking@getmytaxi.in"> booking@getmytaxi.in</a></p>
					</div>
				</div>
				<div class="col-md-4">
					<form action="{{route('addenquiry')}}" method="POST">
						@csrf
						<input type="text" name="name" placeholder="Your Name" class="form-control" required>
						<input type="text" name="email" placeholder="Email Address" class="form-control" required>
						<input type="number" name="number" placeholder="Phone No." class="form-control" required>
						<textarea name="message" placeholder="Message/Comment" class="fom-control" style=""></textarea>
						<input type="submit" value="Send Message" class="form-control btn btn-info">
					</form> 
				</div>
				<div class="col-md-4">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3889.0315282792703!2d77.5940547141346!3d12.905694219814503!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae1510a9cf9a6b%3A0xca1c500378b13d4b!2s16th%20Cross%20Rd%20%26%207th%20B%20Main%20Rd%2C%20JP%20Nagar%204th%20Phase%2C%20Dollar%20Layout%2C%20Phase%204%2C%20J.%20P.%20Nagar%2C%20Bengaluru%2C%20Karnataka%20560078!5e0!3m2!1sen!2sin!4v1603830110023!5m2!1sen!2sin" width="100%" height="250" frameborder="0" style="border:0;margin-top:20px;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
				</div>
			</div>
		</div>
    </div>
</section>

<footer id="footer" role="contentinfo">
		<div class="container">
		
		<div class="row">
			<div class="col-md-12col-xs-12 col-xs-offset-0">
				
					<div class="footer-desc text-center">
						<h3>Get My Taxi Makes Your Ride Hassle Free!!!"</h3>
						<p>We Keep in pace with changing needs of customers to make customer Safety and Comfort in Travel.</p>
					</div>	
				
			</div>
			</div>
		
			<div class="row row-bottom-padded-sm text-center">
				<div class="col-md-12 col-xs-12">
				<p class="btn btn-info call" style="text-transform: uppercase;">Call Us :  <a href="tel:+918884488255" style="color:white;">+918884488255 </a> /  <a href="tel:+918884488355" style="color:white;">+918884488355 </a></p>
				</div>
				<div class="col-md-8 col-md-offset-2 col-xs-12 col-xs-offset-0 text-center">
					<p class="copyright text-center" style="margin-top:10px;">© GET MY TAXI 2020. All Rights Reserved.<br>
 </p>
				</div>
			</div>
			<div class="row row-bottom-padded-sm">
				<div class="col-md-12 text-center">
					<ul class="social social-circle">
						<li><a href="https://www.facebook.com/Get-My-Taxi-100649348516659" target="blank"><i class="fa fa-facebook"></i></a></li>
						<li><a href="https://twitter.com/GetMyTaxi1" target="blank"><i class="fa fa-twitter"></i></a></li>
						<li><a href="https://www.instagram.com/info.getmytaxi/?fbclid=IwAR2zM80_pwvX6kmvPYI_iQG0xC7Eycod51fGJLzZIH1PR1ZybJToFfHSChw" target="blank"><i class="fa fa-instagram"></i></a></li>
						<li><a href="https://www.linkedin.com/company/69648828/admin/" target="blank"><i class="fa fa-linkedin"></i></a></li>
					</ul>
				</div>
			</div>
			

			
		</div>
	</footer>
    <!--Main Footer-->
    <footer class="main-footer">
    	
        
        <!--Footer Bottom-->
         <div class="footer-bottom">
         	<div class="auto-container">
            	<div class="row clearfix">
                	<div class="col-md-6 col-sm-6 col-xs-12">
                    	<ul class="footer-nav">
                        	<li><a href="{{route('pap')}}">Privacy & Policy</a></li>
                        	<li><a href="{{route('tac')}}">Terms & Conditions</a></li>
                            <li><a href="{{route('contact')}}">Contact Us</a></li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 text-right">
                    	<div class="copyright">Copyright &copy; <?php echo date('Y'); ?> All Rights Reserved . Powered By <a href="http://svswebservices.com/" target="blank"> SVS WEB SERVICES </a></div>
                    </div>
                </div>
            </div>
        </div>
    </footer>	
</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-long-arrow-up"></span></div>

<script src="{{asset('js/jquery.js')}}"></script> 
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/jquery-ui.js')}}"></script>
<script src="{{asset('js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<script src="{{asset('js/revolution.min.js')}}"></script>
<script src="{{asset('js/jquery.fancybox.pack.js')}}"></script>
<script src="{{asset('js/jquery.fancybox-media.js')}}"></script>
<script src="{{asset('js/owl.js')}}"></script>
<script src="{{asset('js/wow.js')}}"></script>
<script src="{{asset('js/script.js')}}"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- Google Maps JavaScript library -->
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyBPWm-93yOSzT23NZETV4drFkgJSxajGc4"></script>
	<script>
var pickup = 'pickup';
 
$(document).ready(function () {
 var autocomplete;
 autocomplete = new google.maps.places.Autocomplete((document.getElementById(pickup)), {
  types: ['geocode'],
  /*componentRestrictions: {
   country: "USA"
  }*/
 });
  
 google.maps.event.addListener(autocomplete, 'place_changed', function () {
  var near_place = autocomplete.getPlace();
 });
});


var drop = 'drop';
 
$(document).ready(function () {
 var autocomplete;
 autocomplete = new google.maps.places.Autocomplete((document.getElementById(drop)), {
  types: ['geocode'],
  /*componentRestrictions: {
   country: "USA"
  }*/
 });
  
 google.maps.event.addListener(autocomplete, 'place_changed', function () {
  var near_place = autocomplete.getPlace();
 });
});
</script>
</body>
</html>
