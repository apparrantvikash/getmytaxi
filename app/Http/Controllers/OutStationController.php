<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Outstationtaxi;
class OutStationController extends Controller
{
    function index(){
    	return view('/Out-Station');
    }
    function outStationTaxi(Request $request){
    	$addressFrom = $request->pickup;
		$addressTo = $request->drop;
		$origin      = urlencode($addressFrom);
		$destination = urlencode($addressTo);

		$url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=$origin&destinations=$destination&key=AIzaSyBPWm-93yOSzT23NZETV4drFkgJSxajGc4";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$response = curl_exec($ch);
		curl_close($ch);
		$response_a = json_decode($response, true);
		$total= $response_a['rows'][0]['elements'][0]['distance']['text'];
		$totald = rtrim($total ," km");
		$totald = str_replace( ',', '', $totald);
		$total_distance = ceil($totald);
    	$taxis = Outstationtaxi::orderBy('price' , 'ASC')->get();
    	return view('Out-Station-Taxi' , ['taxis'=>$taxis , 'request'=>$request , 'total_distance'=>$total_distance]);
    }
}
