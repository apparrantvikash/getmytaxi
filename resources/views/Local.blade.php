@extends('master')
@section('content')

    <!--Main Banner-->
	<section style="background:url('images/main-slider/image-2.jpg');padding: 50px 0 ;background-size: cover;">
		<div class="auto-container">
			<div class="banner">
				<center>
				<h1 class="top_title">Book a Local Taxi to your destination in town</h1>
				<h5 class="top_subtitle">Choose from a range of categories and prices</h5>
				</center>
			</div>
			<div class="row">
				<div class="col-md-5">
					<div class="booking_form">
						<div class="border">
							<div class="row">
								<div class="col-md-12"><div class="aro" style="background:#00060b;"><a style="color:white;">Local Taxi</a></div></div>
							</div>
						</div>
						<div class="bfcontent">
							<center><h4>Your everyday travel partner</h4>
							<h5>Your everyday travel partner</h5></center>
						</div>
						<form method="POST" action="{{route('localtaxi')}}">
							@csrf
							<span>PICKUP</span><input type="text" name="pickup" placeholder="Bangalore, Karnataka, India"  id="pickup"  required >
							<span>DROP </span><input type="text" name="drop" placeholder="Enter drop for ride estimate"  id="drop" required>
							<span>DATE </span><input type="date" name="date" placeholder="Date"  required>
							<span>TIME </span><input type="time" name="time" placeholder="Time"  required>
							<button type="submit">Search Taxi</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
    <!--End Main Banner-->
@endsection